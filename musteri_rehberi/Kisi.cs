﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace musteri_rehberi
{
    public class Kisi
    {

        public Guid ID { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string TelefonNo { get; set; }
        public string Uyruk { get; set; }
        public string eposta { get; set; }
        public string AracTipi { get; set; }
        public int donusKm { get; set; }
        public string plaka { get; set; }
        public string teslimDurumu { get; set; }
        public DateTime baslangicTarihi { get; set; }
        public DateTime bitisTarihi { get; set; }
        public string paraBirimi { get; set; }
        public string kiralamaSuresi { get; set; }
        public string kiralamaUcreti { get; set; }
        public string ekstraUcretler { get; set; }
        public string toplamUcret { get; set; }



    }
}
