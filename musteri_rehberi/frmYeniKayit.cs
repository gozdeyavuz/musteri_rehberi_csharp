﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace musteri_rehberi
{
    public partial class frmYeniKayit : Form
    {

        public Kisi YeniKisi { get; set; }

        public frmYeniKayit()
        {
            InitializeComponent();
        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            this.YeniKisi = new Kisi();

            this.YeniKisi.ID = Guid.NewGuid();
            this.YeniKisi.Ad = this.txtMusteriAdi.Text;
            this.YeniKisi.Soyad = this.txtMusteriSoyadi.Text;
            this.YeniKisi.Uyruk = this.txtUyruk.Text;
            this.YeniKisi.TelefonNo = this.txtTelNo.Text;
            this.YeniKisi.eposta = this.txtMail.Text;
            this.YeniKisi.AracTipi = this.txtAracTipi.Text;
            this.YeniKisi.donusKm = Convert.ToInt32(this.txtDonusKm.Text);
            this.YeniKisi.teslimDurumu = this.txtTeslimDurumu.Text;
            this.YeniKisi.plaka = this.txtPlaka.Text;
            this.YeniKisi.baslangicTarihi = this.dTBaslangicTarihi.Value;
            this.YeniKisi.bitisTarihi = this.dTBitisTarihi.Value;
            if(this.rButtonTL.Checked)
            { this.YeniKisi.paraBirimi = this.rButtonTL.Text; }
            else if (this.rButtonEuro.Checked)
            { this.YeniKisi.paraBirimi = this.rButtonEuro.Text; }
            else if(this.rButtonDolar.Checked)
            { this.YeniKisi.paraBirimi = this.rButtonDolar.Text; }
            this.YeniKisi.kiralamaSuresi = this.txtKiralamaSuresi.Text;
            this.YeniKisi.kiralamaUcreti = this.txtKiralamaUcreti.Text;
            this.YeniKisi.ekstraUcretler = this.txtEkstraUcretler.Text;
            this.YeniKisi.toplamUcret = this.lblToplamUcret.Text;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void rButtonEuro_CheckedChanged(object sender, EventArgs e)
        {
            lblToplamParaBirimi.Text = "€";

        }

        private void rButtonTL_CheckedChanged(object sender, EventArgs e)
        {
            lblToplamParaBirimi.Text = "TL";
        }

        private void rButtonDolar_CheckedChanged(object sender, EventArgs e)
        {
            lblToplamParaBirimi.Text = "$";
        }

        private void btnİptal_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void txtEkstraUcretler_TextChanged(object sender, EventArgs e)
        {
            int toplam = Convert.ToInt32(txtEkstraUcretler.Text) + Convert.ToInt32(txtKiralamaUcreti.Text);
            lblToplamUcret.Text = toplam.ToString();
        }
    }
}
