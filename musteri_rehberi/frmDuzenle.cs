﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace musteri_rehberi
{
    public partial class frmDuzenle : Form
    {
        public Kisi guncelleme { get; set; }
        public frmDuzenle()
        {
            InitializeComponent();
        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {

            this.guncelleme.Ad = this.txtMusteriAdi.Text;
            this.guncelleme.Soyad = this.txtMusteriSoyadi.Text;
            this.guncelleme.Uyruk = this.txtUyruk.Text;
            this.guncelleme.TelefonNo = this.txtTelNo.Text;
            this.guncelleme.eposta = this.txtMail.Text;
            this.guncelleme.AracTipi = this.txtAracTipi.Text;
            this.guncelleme.donusKm = Convert.ToInt32(this.txtDonusKm.Text);
            this.guncelleme.teslimDurumu = this.txtTeslimDurumu.Text;
            this.guncelleme.plaka = this.txtPlaka.Text;
            this.guncelleme.baslangicTarihi = this.dTBaslangicTarihi.Value;
            this.guncelleme.bitisTarihi = this.dTBitisTarihi.Value;
            if (this.rButtonTL.Checked)
            { this.guncelleme.paraBirimi = this.rButtonTL.Text; }
            else if (this.rButtonEuro.Checked)
            { this.guncelleme.paraBirimi = this.rButtonEuro.Text; }
            else if (this.rButtonDolar.Checked)
            { this.guncelleme.paraBirimi = this.rButtonDolar.Text; }
            this.guncelleme.kiralamaSuresi = this.txtKiralamaSuresi.Text;
            this.guncelleme.kiralamaUcreti = this.txtKiralamaUcreti.Text;
            this.guncelleme.ekstraUcretler = this.txtEkstraUcretler.Text;
            this.guncelleme.toplamUcret = this.lblToplamUcret.Text;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnİptal_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void frmDuzenle_Load(object sender, EventArgs e)
        {
            if(guncelleme != null)
            {
                this.txtMusteriAdi.Text = this.guncelleme.Ad;
                this.txtMusteriSoyadi.Text = this.guncelleme.Soyad;
                this.txtUyruk.Text = this.guncelleme.Uyruk;
                this.txtTelNo.Text = this.guncelleme.TelefonNo;
                this.txtMail.Text = this.guncelleme.eposta;
                this.txtAracTipi.Text = this.guncelleme.AracTipi;
                this.txtPlaka.Text = this.guncelleme.plaka;
                this.txtTeslimDurumu.Text = this.guncelleme.teslimDurumu;
                this.txtDonusKm.Text = this.guncelleme.donusKm.ToString();
                this.dTBaslangicTarihi.Text = this.guncelleme.baslangicTarihi.ToString();
                this.dTBitisTarihi.Text = this.guncelleme.bitisTarihi.ToString();
                this.txtKiralamaSuresi.Text = this.guncelleme.kiralamaSuresi;
                this.txtKiralamaUcreti.Text = this.guncelleme.kiralamaUcreti;
                this.lblToplamUcret.Text = this.guncelleme.toplamUcret;
                this.txtEkstraUcretler.Text = this.guncelleme.ekstraUcretler;





            }


        }

        private void rButtonEuro_CheckedChanged(object sender, EventArgs e)
        {
            lblToplamParaBirimi.Text = "€";

        }

        private void rButtonTL_CheckedChanged(object sender, EventArgs e)
        {
            lblToplamParaBirimi.Text = "TL";
        }

        private void rButtonDolar_CheckedChanged(object sender, EventArgs e)
        {
            lblToplamParaBirimi.Text = "$";
        }

  
    }
}
