﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace musteri_rehberi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private DataSet ds = new DataSet();


        private void Form1_Load(object sender, EventArgs e)
        {
            verileriYukle();
        }
      

        private void btnYeniKayit_Click(object sender, EventArgs e)
        {
            
            frmYeniKayit frmYeniKayit = new frmYeniKayit();

            DialogResult sonuc = frmYeniKayit.ShowDialog();

            if(sonuc == System.Windows.Forms.DialogResult.OK)
            {
                Kisi yenikisi = frmYeniKayit.YeniKisi;

                DataTable dt = this.dataGridView1.DataSource as DataTable;

                DataRow dr = dt.NewRow();

                dr[0] = yenikisi.ID;
                dr[1] = yenikisi.Ad;
                dr[2] = yenikisi.Soyad;
                dr[3] = yenikisi.Uyruk;
                dr[4] = yenikisi.TelefonNo;
                dr[5] = yenikisi.eposta;
                dr[6] = yenikisi.AracTipi;
                dr[7] = yenikisi.donusKm;
                dr[8] = yenikisi.plaka;
                dr[9] = yenikisi.teslimDurumu;
                dr[10] = yenikisi.baslangicTarihi;
                dr[11] = yenikisi.bitisTarihi;
                dr[12] = yenikisi.paraBirimi;
                dr[13] = yenikisi.kiralamaSuresi;
                dr[14] = yenikisi.kiralamaUcreti;
                dr[15] = yenikisi.ekstraUcretler;
                dr[16] = yenikisi.toplamUcret;

                dt.Rows.Add(dr);

                this.KayitSayisiHesaplama();
                lblSonİslem.Text = "Yeni kayıt eklendi";

            }
        }

       

        private void btnKayitDuzenle_Click(object sender, EventArgs e)
        {
           
           

            if(this.dataGridView1.CurrentRow != null)
            {
                frmDuzenle frmDuzenle = new frmDuzenle();

                frmDuzenle.guncelleme = new Kisi();

                frmDuzenle.guncelleme.Ad = this.dataGridView1.CurrentRow.Cells[1].Value.ToString();
                frmDuzenle.guncelleme.Soyad = this.dataGridView1.CurrentRow.Cells[2].Value.ToString();
                frmDuzenle.guncelleme.Uyruk = this.dataGridView1.CurrentRow.Cells[3].Value.ToString();
                frmDuzenle.guncelleme.TelefonNo = this.dataGridView1.CurrentRow.Cells[4].Value.ToString();
                frmDuzenle.guncelleme.eposta = this.dataGridView1.CurrentRow.Cells[5].Value.ToString();
                frmDuzenle.guncelleme.AracTipi = this.dataGridView1.CurrentRow.Cells[6].Value.ToString();
                frmDuzenle.guncelleme.donusKm = Convert.ToInt32(this.dataGridView1.CurrentRow.Cells[7].Value);
                frmDuzenle.guncelleme.plaka = this.dataGridView1.CurrentRow.Cells[8].Value.ToString();
                frmDuzenle.guncelleme.teslimDurumu = this.dataGridView1.CurrentRow.Cells[9].Value.ToString();
                frmDuzenle.guncelleme.baslangicTarihi = DateTime.Parse(this.dataGridView1.CurrentRow.Cells[10].Value.ToString());
                frmDuzenle.guncelleme.bitisTarihi = DateTime.Parse(this.dataGridView1.CurrentRow.Cells[11].Value.ToString());
                frmDuzenle.guncelleme.paraBirimi = this.dataGridView1.CurrentRow.Cells[12].Value.ToString();
                frmDuzenle.guncelleme.kiralamaSuresi = this.dataGridView1.CurrentRow.Cells[13].Value.ToString();
                frmDuzenle.guncelleme.kiralamaUcreti = this.dataGridView1.CurrentRow.Cells[14].Value.ToString();
                frmDuzenle.guncelleme.ekstraUcretler = this.dataGridView1.CurrentRow.Cells[15].Value.ToString();
                frmDuzenle.guncelleme.toplamUcret = this.dataGridView1.CurrentRow.Cells[16].Value.ToString();

                DialogResult sonuc = frmDuzenle.ShowDialog();

                if(sonuc == System.Windows.Forms.DialogResult.OK)
                {


                    DataRowView drv = this.dataGridView1.CurrentRow.DataBoundItem as DataRowView;
                    DataRow dr = drv.Row;
                  

                    dr[1] = frmDuzenle.guncelleme.Ad;
                    dr[2] = frmDuzenle.guncelleme.Soyad;
                    dr[3] = frmDuzenle.guncelleme.Uyruk;
                    dr[4] = frmDuzenle.guncelleme.TelefonNo;
                    dr[5] = frmDuzenle.guncelleme.eposta;
                    dr[6] = frmDuzenle.guncelleme.AracTipi;
                    dr[7] = frmDuzenle.guncelleme.donusKm;
                    dr[8] = frmDuzenle.guncelleme.plaka;
                    dr[9] = frmDuzenle.guncelleme.teslimDurumu;
                    dr[10] = frmDuzenle.guncelleme.baslangicTarihi;
                    dr[11] = frmDuzenle.guncelleme.bitisTarihi;
                    dr[12] = frmDuzenle.guncelleme.paraBirimi;
                    dr[13] = frmDuzenle.guncelleme.kiralamaSuresi;
                    dr[14] = frmDuzenle.guncelleme.kiralamaUcreti;
                    dr[15] = frmDuzenle.guncelleme.ekstraUcretler;
                    dr[16] = frmDuzenle.guncelleme.toplamUcret;

                    lblSonİslem.Text = "Güncelleme yapıldı.";

                }



            }


        }

        private void btnKayitSilme_Click(object sender, EventArgs e)
        {   

            if(this.dataGridView1.CurrentRow != null)
            {
                DialogResult sonuc = MessageBox.Show("Seçili kayıt silinecektir. Silmek istediğinizden emin misiniz?", "Kayıt Silme İşlemi", MessageBoxButtons.OKCancel);

                if (sonuc == System.Windows.Forms.DialogResult.OK)
                {

                    DataRowView drv = this.dataGridView1.CurrentRow.DataBoundItem as DataRowView;
                    DataRow dr = drv.Row;
                    dr.Delete();
                    this.KayitSayisiHesaplama();
                    lblSonİslem.Text = "Kayıt silindi.";

                }
            }
           
        }

        private void btnKaydet_Click(object sender, EventArgs e) 
        {
            ds.WriteXml(Application.StartupPath + "\\" + "data.xml", XmlWriteMode.WriteSchema);
            lblSonİslem.Text = "Kaydedildi.";
        }

        private void menuYenile_Click(object sender, EventArgs e)
        {
            verileriYukle();
        }
        private void KayitSayisiHesaplama()
        {

            this.lblKayitSayisi.Text = "Kayıt Sayısı :" +  this.dataGridView1.RowCount.ToString() ;

        }

        private void TmrSaat_Tick(object sender, EventArgs e)
        {
            lblSaat.Text = System.DateTime.Now.ToString();
        }

        private void verileriYukle()

        {
            tmrSaat.Start();
            tmrSaat.Tick += TmrSaat_Tick;
            //DataTable dt = new DataTable("Kisiler");

            //DataColumn dcID = new DataColumn("ID");
            //DataColumn dcAd = new DataColumn("Musteri Adi");
            //DataColumn dcSoyad = new DataColumn("Musteri Soyadi");
            //DataColumn dcUyruk = new DataColumn("Uyruk");
            //DataColumn dcTelno = new DataColumn("Telefon No");
            //DataColumn dcEposta = new DataColumn("E Posta");
            //DataColumn dcAracTipi = new DataColumn("Arac Tipi");
            //DataColumn dcDonucKm = new DataColumn("Donus Kilometresi");
            //DataColumn dcPlaka = new DataColumn("Plaka");
            //DataColumn dcTeslimDurumu = new DataColumn("Teslim Durumu");
            //DataColumn dcBaslangicTarihi = new DataColumn("Baslangic Tarihi");
            //DataColumn gcBitisTarihi = new DataColumn("Bitis Tarihi");
            //DataColumn dcParaBirimi = new DataColumn("Para Birimi");
            //DataColumn dcKiralamaSuresi = new DataColumn("Kiralama Suresi");
            //DataColumn dcKiralamaUcreti = new DataColumn("Kiralama Ucreti");
            //DataColumn dcEkstraUcretler = new DataColumn("Ekstra Ucretler");
            //DataColumn dcToplam = new DataColumn("Toplam Tutar");

            //dt.Columns.Add(dcID);
            //dt.Columns.Add(dcAd);
            //dt.Columns.Add(dcSoyad);
            //dt.Columns.Add(dcUyruk);
            //dt.Columns.Add(dcTelno);
            //dt.Columns.Add(dcEposta);
            //dt.Columns.Add(dcAracTipi);
            //dt.Columns.Add(dcDonucKm);
            //dt.Columns.Add(dcPlaka);
            //dt.Columns.Add(dcTeslimDurumu);
            //dt.Columns.Add(dcBaslangicTarihi);
            //dt.Columns.Add(gcBitisTarihi);
            //dt.Columns.Add(dcParaBirimi);
            //dt.Columns.Add(dcKiralamaSuresi);
            //dt.Columns.Add(dcKiralamaUcreti);
            //dt.Columns.Add(dcEkstraUcretler);
            //dt.Columns.Add(dcToplam);

            //this.dataGridView1.DataSource = dt;

            ds.Tables.Clear();
            ds.ReadXml(Application.StartupPath + "\\" + "data.xml", XmlReadMode.ReadSchema);

            if (ds.Tables.Count > 0)
            {
                this.dataGridView1.DataSource = ds.Tables[0];
                this.KayitSayisiHesaplama();
                lblSonİslem.Text = "Veriler yüklendi";

            }
        }

        private void menuCikis_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}

