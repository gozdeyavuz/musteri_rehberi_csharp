﻿namespace musteri_rehberi
{
    partial class frmDuzenle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDuzenle));
            this.btnİptal = new System.Windows.Forms.Button();
            this.lblToplamUcret = new System.Windows.Forms.Label();
            this.lblToplamParaBirimi = new System.Windows.Forms.Label();
            this.rButtonTL = new System.Windows.Forms.RadioButton();
            this.rButtonDolar = new System.Windows.Forms.RadioButton();
            this.rButtonEuro = new System.Windows.Forms.RadioButton();
            this.lblParaBirimi = new System.Windows.Forms.Label();
            this.lblToplam = new System.Windows.Forms.Label();
            this.dTBitisTarihi = new System.Windows.Forms.DateTimePicker();
            this.pnlKiralamaBilgileri = new System.Windows.Forms.Panel();
            this.dTBaslangicTarihi = new System.Windows.Forms.DateTimePicker();
            this.txtKiralamaUcreti = new System.Windows.Forms.TextBox();
            this.txtKiralamaSuresi = new System.Windows.Forms.TextBox();
            this.txtEkstraUcretler = new System.Windows.Forms.TextBox();
            this.lblEkstraUcretler = new System.Windows.Forms.Label();
            this.lblKiralamaSuresi = new System.Windows.Forms.Label();
            this.lblBaslangicTarihi = new System.Windows.Forms.Label();
            this.lblBitisTarihi = new System.Windows.Forms.Label();
            this.lblKiralamaUcreti = new System.Windows.Forms.Label();
            this.lblKiralamaBilgileri = new System.Windows.Forms.Label();
            this.btnKaydet = new System.Windows.Forms.Button();
            this.lblAracBilgileri = new System.Windows.Forms.Label();
            this.pnlMüsteriBilgileri = new System.Windows.Forms.Panel();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.txtMusteriSoyadi = new System.Windows.Forms.TextBox();
            this.txtTelNo = new System.Windows.Forms.TextBox();
            this.txtUyruk = new System.Windows.Forms.TextBox();
            this.txtMusteriAdi = new System.Windows.Forms.TextBox();
            this.lblUyruk = new System.Windows.Forms.Label();
            this.lblMail = new System.Windows.Forms.Label();
            this.lblTelNo = new System.Windows.Forms.Label();
            this.lblMüsteriSoyadi = new System.Windows.Forms.Label();
            this.lblMüsteriAdi = new System.Windows.Forms.Label();
            this.lblAracTipi = new System.Windows.Forms.Label();
            this.pnlAracBilgileri = new System.Windows.Forms.Panel();
            this.txtTeslimDurumu = new System.Windows.Forms.TextBox();
            this.txtDonusKm = new System.Windows.Forms.TextBox();
            this.txtPlaka = new System.Windows.Forms.TextBox();
            this.txtAracTipi = new System.Windows.Forms.TextBox();
            this.lblTeslimDurumu = new System.Windows.Forms.Label();
            this.lblDonusKm = new System.Windows.Forms.Label();
            this.lblPlaka = new System.Windows.Forms.Label();
            this.lblMüsteriBilgileri = new System.Windows.Forms.Label();
            this.pnlKiralamaBilgileri.SuspendLayout();
            this.pnlMüsteriBilgileri.SuspendLayout();
            this.pnlAracBilgileri.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnİptal
            // 
            this.btnİptal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnİptal.Location = new System.Drawing.Point(353, 579);
            this.btnİptal.Name = "btnİptal";
            this.btnİptal.Size = new System.Drawing.Size(75, 23);
            this.btnİptal.TabIndex = 14;
            this.btnİptal.Text = "İptal";
            this.btnİptal.UseVisualStyleBackColor = true;
            this.btnİptal.Click += new System.EventHandler(this.btnİptal_Click);
            // 
            // lblToplamUcret
            // 
            this.lblToplamUcret.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblToplamUcret.AutoSize = true;
            this.lblToplamUcret.Location = new System.Drawing.Point(339, 164);
            this.lblToplamUcret.Name = "lblToplamUcret";
            this.lblToplamUcret.Size = new System.Drawing.Size(43, 13);
            this.lblToplamUcret.TabIndex = 18;
            this.lblToplamUcret.Text = "______";
            // 
            // lblToplamParaBirimi
            // 
            this.lblToplamParaBirimi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblToplamParaBirimi.AutoSize = true;
            this.lblToplamParaBirimi.Location = new System.Drawing.Point(388, 164);
            this.lblToplamParaBirimi.Name = "lblToplamParaBirimi";
            this.lblToplamParaBirimi.Size = new System.Drawing.Size(13, 13);
            this.lblToplamParaBirimi.TabIndex = 17;
            this.lblToplamParaBirimi.Text = "€";
            // 
            // rButtonTL
            // 
            this.rButtonTL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rButtonTL.AutoSize = true;
            this.rButtonTL.Location = new System.Drawing.Point(206, 83);
            this.rButtonTL.Name = "rButtonTL";
            this.rButtonTL.Size = new System.Drawing.Size(74, 17);
            this.rButtonTL.TabIndex = 16;
            this.rButtonTL.TabStop = true;
            this.rButtonTL.Text = "Türk Lirası";
            this.rButtonTL.UseVisualStyleBackColor = true;
            this.rButtonTL.CheckedChanged += new System.EventHandler(this.rButtonTL_CheckedChanged);
            // 
            // rButtonDolar
            // 
            this.rButtonDolar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rButtonDolar.AutoSize = true;
            this.rButtonDolar.Location = new System.Drawing.Point(308, 83);
            this.rButtonDolar.Name = "rButtonDolar";
            this.rButtonDolar.Size = new System.Drawing.Size(50, 17);
            this.rButtonDolar.TabIndex = 15;
            this.rButtonDolar.TabStop = true;
            this.rButtonDolar.Text = "Dolar";
            this.rButtonDolar.UseVisualStyleBackColor = true;
            this.rButtonDolar.CheckedChanged += new System.EventHandler(this.rButtonDolar_CheckedChanged);
            // 
            // rButtonEuro
            // 
            this.rButtonEuro.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rButtonEuro.AutoSize = true;
            this.rButtonEuro.Location = new System.Drawing.Point(126, 83);
            this.rButtonEuro.Name = "rButtonEuro";
            this.rButtonEuro.Size = new System.Drawing.Size(47, 17);
            this.rButtonEuro.TabIndex = 14;
            this.rButtonEuro.TabStop = true;
            this.rButtonEuro.Text = "Euro";
            this.rButtonEuro.UseVisualStyleBackColor = true;
            this.rButtonEuro.CheckedChanged += new System.EventHandler(this.rButtonEuro_CheckedChanged);
            // 
            // lblParaBirimi
            // 
            this.lblParaBirimi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblParaBirimi.AutoSize = true;
            this.lblParaBirimi.Location = new System.Drawing.Point(23, 85);
            this.lblParaBirimi.Name = "lblParaBirimi";
            this.lblParaBirimi.Size = new System.Drawing.Size(62, 13);
            this.lblParaBirimi.TabIndex = 13;
            this.lblParaBirimi.Text = "Para Birimi :";
            // 
            // lblToplam
            // 
            this.lblToplam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblToplam.AutoSize = true;
            this.lblToplam.Location = new System.Drawing.Point(249, 164);
            this.lblToplam.Name = "lblToplam";
            this.lblToplam.Size = new System.Drawing.Size(77, 13);
            this.lblToplam.TabIndex = 10;
            this.lblToplam.Text = "Toplam Ücret :";
            // 
            // dTBitisTarihi
            // 
            this.dTBitisTarihi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dTBitisTarihi.Location = new System.Drawing.Point(125, 46);
            this.dTBitisTarihi.Name = "dTBitisTarihi";
            this.dTBitisTarihi.Size = new System.Drawing.Size(172, 20);
            this.dTBitisTarihi.TabIndex = 9;
            // 
            // pnlKiralamaBilgileri
            // 
            this.pnlKiralamaBilgileri.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlKiralamaBilgileri.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlKiralamaBilgileri.Controls.Add(this.lblToplamUcret);
            this.pnlKiralamaBilgileri.Controls.Add(this.lblToplamParaBirimi);
            this.pnlKiralamaBilgileri.Controls.Add(this.rButtonTL);
            this.pnlKiralamaBilgileri.Controls.Add(this.rButtonDolar);
            this.pnlKiralamaBilgileri.Controls.Add(this.rButtonEuro);
            this.pnlKiralamaBilgileri.Controls.Add(this.lblParaBirimi);
            this.pnlKiralamaBilgileri.Controls.Add(this.lblToplam);
            this.pnlKiralamaBilgileri.Controls.Add(this.dTBitisTarihi);
            this.pnlKiralamaBilgileri.Controls.Add(this.dTBaslangicTarihi);
            this.pnlKiralamaBilgileri.Controls.Add(this.txtKiralamaUcreti);
            this.pnlKiralamaBilgileri.Controls.Add(this.txtKiralamaSuresi);
            this.pnlKiralamaBilgileri.Controls.Add(this.txtEkstraUcretler);
            this.pnlKiralamaBilgileri.Controls.Add(this.lblEkstraUcretler);
            this.pnlKiralamaBilgileri.Controls.Add(this.lblKiralamaSuresi);
            this.pnlKiralamaBilgileri.Controls.Add(this.lblBaslangicTarihi);
            this.pnlKiralamaBilgileri.Controls.Add(this.lblBitisTarihi);
            this.pnlKiralamaBilgileri.Controls.Add(this.lblKiralamaUcreti);
            this.pnlKiralamaBilgileri.Location = new System.Drawing.Point(32, 361);
            this.pnlKiralamaBilgileri.Name = "pnlKiralamaBilgileri";
            this.pnlKiralamaBilgileri.Size = new System.Drawing.Size(477, 199);
            this.pnlKiralamaBilgileri.TabIndex = 12;
            // 
            // dTBaslangicTarihi
            // 
            this.dTBaslangicTarihi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dTBaslangicTarihi.Location = new System.Drawing.Point(125, 17);
            this.dTBaslangicTarihi.Name = "dTBaslangicTarihi";
            this.dTBaslangicTarihi.Size = new System.Drawing.Size(172, 20);
            this.dTBaslangicTarihi.TabIndex = 8;
            // 
            // txtKiralamaUcreti
            // 
            this.txtKiralamaUcreti.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKiralamaUcreti.Location = new System.Drawing.Point(126, 157);
            this.txtKiralamaUcreti.Name = "txtKiralamaUcreti";
            this.txtKiralamaUcreti.Size = new System.Drawing.Size(100, 20);
            this.txtKiralamaUcreti.TabIndex = 7;
            // 
            // txtKiralamaSuresi
            // 
            this.txtKiralamaSuresi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKiralamaSuresi.Location = new System.Drawing.Point(126, 120);
            this.txtKiralamaSuresi.Name = "txtKiralamaSuresi";
            this.txtKiralamaSuresi.Size = new System.Drawing.Size(100, 20);
            this.txtKiralamaSuresi.TabIndex = 6;
            // 
            // txtEkstraUcretler
            // 
            this.txtEkstraUcretler.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEkstraUcretler.Location = new System.Drawing.Point(337, 116);
            this.txtEkstraUcretler.Name = "txtEkstraUcretler";
            this.txtEkstraUcretler.Size = new System.Drawing.Size(100, 20);
            this.txtEkstraUcretler.TabIndex = 5;
            // 
            // lblEkstraUcretler
            // 
            this.lblEkstraUcretler.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEkstraUcretler.AutoSize = true;
            this.lblEkstraUcretler.Location = new System.Drawing.Point(249, 123);
            this.lblEkstraUcretler.Name = "lblEkstraUcretler";
            this.lblEkstraUcretler.Size = new System.Drawing.Size(83, 13);
            this.lblEkstraUcretler.TabIndex = 4;
            this.lblEkstraUcretler.Text = "Ekstra Ücretler :";
            // 
            // lblKiralamaSuresi
            // 
            this.lblKiralamaSuresi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblKiralamaSuresi.AutoSize = true;
            this.lblKiralamaSuresi.Location = new System.Drawing.Point(23, 123);
            this.lblKiralamaSuresi.Name = "lblKiralamaSuresi";
            this.lblKiralamaSuresi.Size = new System.Drawing.Size(79, 13);
            this.lblKiralamaSuresi.TabIndex = 3;
            this.lblKiralamaSuresi.Text = "Kiralama Süresi";
            // 
            // lblBaslangicTarihi
            // 
            this.lblBaslangicTarihi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBaslangicTarihi.AutoSize = true;
            this.lblBaslangicTarihi.Location = new System.Drawing.Point(23, 24);
            this.lblBaslangicTarihi.Name = "lblBaslangicTarihi";
            this.lblBaslangicTarihi.Size = new System.Drawing.Size(88, 13);
            this.lblBaslangicTarihi.TabIndex = 2;
            this.lblBaslangicTarihi.Text = "Başlangıç Tarihi :";
            // 
            // lblBitisTarihi
            // 
            this.lblBitisTarihi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBitisTarihi.AutoSize = true;
            this.lblBitisTarihi.Location = new System.Drawing.Point(23, 52);
            this.lblBitisTarihi.Name = "lblBitisTarihi";
            this.lblBitisTarihi.Size = new System.Drawing.Size(61, 13);
            this.lblBitisTarihi.TabIndex = 1;
            this.lblBitisTarihi.Text = "Bitiş Tarihi :";
            // 
            // lblKiralamaUcreti
            // 
            this.lblKiralamaUcreti.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblKiralamaUcreti.AutoSize = true;
            this.lblKiralamaUcreti.Location = new System.Drawing.Point(23, 164);
            this.lblKiralamaUcreti.Name = "lblKiralamaUcreti";
            this.lblKiralamaUcreti.Size = new System.Drawing.Size(78, 13);
            this.lblKiralamaUcreti.TabIndex = 0;
            this.lblKiralamaUcreti.Text = "Kiralama Ucreti";
            // 
            // lblKiralamaBilgileri
            // 
            this.lblKiralamaBilgileri.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblKiralamaBilgileri.AutoSize = true;
            this.lblKiralamaBilgileri.Location = new System.Drawing.Point(33, 352);
            this.lblKiralamaBilgileri.Name = "lblKiralamaBilgileri";
            this.lblKiralamaBilgileri.Size = new System.Drawing.Size(82, 13);
            this.lblKiralamaBilgileri.TabIndex = 13;
            this.lblKiralamaBilgileri.Text = "Kiralama Bilgileri";
            // 
            // btnKaydet
            // 
            this.btnKaydet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKaydet.Location = new System.Drawing.Point(434, 579);
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.Size = new System.Drawing.Size(75, 23);
            this.btnKaydet.TabIndex = 15;
            this.btnKaydet.Text = "Kaydet";
            this.btnKaydet.UseVisualStyleBackColor = true;
            this.btnKaydet.Click += new System.EventHandler(this.btnKaydet_Click);
            // 
            // lblAracBilgileri
            // 
            this.lblAracBilgileri.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAracBilgileri.AutoSize = true;
            this.lblAracBilgileri.Location = new System.Drawing.Point(32, 212);
            this.lblAracBilgileri.Name = "lblAracBilgileri";
            this.lblAracBilgileri.Size = new System.Drawing.Size(64, 13);
            this.lblAracBilgileri.TabIndex = 11;
            this.lblAracBilgileri.Text = "Araç Bilgileri";
            // 
            // pnlMüsteriBilgileri
            // 
            this.pnlMüsteriBilgileri.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlMüsteriBilgileri.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMüsteriBilgileri.Controls.Add(this.txtMail);
            this.pnlMüsteriBilgileri.Controls.Add(this.txtMusteriSoyadi);
            this.pnlMüsteriBilgileri.Controls.Add(this.txtTelNo);
            this.pnlMüsteriBilgileri.Controls.Add(this.txtUyruk);
            this.pnlMüsteriBilgileri.Controls.Add(this.txtMusteriAdi);
            this.pnlMüsteriBilgileri.Controls.Add(this.lblUyruk);
            this.pnlMüsteriBilgileri.Controls.Add(this.lblMail);
            this.pnlMüsteriBilgileri.Controls.Add(this.lblTelNo);
            this.pnlMüsteriBilgileri.Controls.Add(this.lblMüsteriSoyadi);
            this.pnlMüsteriBilgileri.Controls.Add(this.lblMüsteriAdi);
            this.pnlMüsteriBilgileri.Location = new System.Drawing.Point(32, 29);
            this.pnlMüsteriBilgileri.Name = "pnlMüsteriBilgileri";
            this.pnlMüsteriBilgileri.Size = new System.Drawing.Size(477, 157);
            this.pnlMüsteriBilgileri.TabIndex = 8;
            this.pnlMüsteriBilgileri.Tag = "";
            // 
            // txtMail
            // 
            this.txtMail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMail.Location = new System.Drawing.Point(337, 106);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(100, 20);
            this.txtMail.TabIndex = 9;
            // 
            // txtMusteriSoyadi
            // 
            this.txtMusteriSoyadi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMusteriSoyadi.Location = new System.Drawing.Point(337, 27);
            this.txtMusteriSoyadi.Name = "txtMusteriSoyadi";
            this.txtMusteriSoyadi.Size = new System.Drawing.Size(100, 20);
            this.txtMusteriSoyadi.TabIndex = 8;
            // 
            // txtTelNo
            // 
            this.txtTelNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTelNo.Location = new System.Drawing.Point(125, 106);
            this.txtTelNo.Name = "txtTelNo";
            this.txtTelNo.Size = new System.Drawing.Size(100, 20);
            this.txtTelNo.TabIndex = 7;
            // 
            // txtUyruk
            // 
            this.txtUyruk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUyruk.Location = new System.Drawing.Point(125, 67);
            this.txtUyruk.Name = "txtUyruk";
            this.txtUyruk.Size = new System.Drawing.Size(100, 20);
            this.txtUyruk.TabIndex = 6;
            // 
            // txtMusteriAdi
            // 
            this.txtMusteriAdi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMusteriAdi.Location = new System.Drawing.Point(125, 27);
            this.txtMusteriAdi.Name = "txtMusteriAdi";
            this.txtMusteriAdi.Size = new System.Drawing.Size(100, 20);
            this.txtMusteriAdi.TabIndex = 5;
            // 
            // lblUyruk
            // 
            this.lblUyruk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUyruk.AutoSize = true;
            this.lblUyruk.Location = new System.Drawing.Point(23, 74);
            this.lblUyruk.Name = "lblUyruk";
            this.lblUyruk.Size = new System.Drawing.Size(41, 13);
            this.lblUyruk.TabIndex = 4;
            this.lblUyruk.Text = "Uyruk :";
            // 
            // lblMail
            // 
            this.lblMail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMail.AutoSize = true;
            this.lblMail.Location = new System.Drawing.Point(249, 113);
            this.lblMail.Name = "lblMail";
            this.lblMail.Size = new System.Drawing.Size(82, 13);
            this.lblMail.TabIndex = 3;
            this.lblMail.Text = "E-Posta Adresi :";
            // 
            // lblTelNo
            // 
            this.lblTelNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTelNo.AutoSize = true;
            this.lblTelNo.Location = new System.Drawing.Point(23, 113);
            this.lblTelNo.Name = "lblTelNo";
            this.lblTelNo.Size = new System.Drawing.Size(96, 13);
            this.lblTelNo.TabIndex = 2;
            this.lblTelNo.Text = "Telefon Numarası :";
            // 
            // lblMüsteriSoyadi
            // 
            this.lblMüsteriSoyadi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMüsteriSoyadi.AutoSize = true;
            this.lblMüsteriSoyadi.Location = new System.Drawing.Point(249, 34);
            this.lblMüsteriSoyadi.Name = "lblMüsteriSoyadi";
            this.lblMüsteriSoyadi.Size = new System.Drawing.Size(82, 13);
            this.lblMüsteriSoyadi.TabIndex = 1;
            this.lblMüsteriSoyadi.Text = "Müşteri Soyadı :";
            // 
            // lblMüsteriAdi
            // 
            this.lblMüsteriAdi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMüsteriAdi.AutoSize = true;
            this.lblMüsteriAdi.Location = new System.Drawing.Point(23, 34);
            this.lblMüsteriAdi.Name = "lblMüsteriAdi";
            this.lblMüsteriAdi.Size = new System.Drawing.Size(65, 13);
            this.lblMüsteriAdi.TabIndex = 0;
            this.lblMüsteriAdi.Text = "Müşteri Adı :";
            // 
            // lblAracTipi
            // 
            this.lblAracTipi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAracTipi.AutoSize = true;
            this.lblAracTipi.Location = new System.Drawing.Point(24, 24);
            this.lblAracTipi.Name = "lblAracTipi";
            this.lblAracTipi.Size = new System.Drawing.Size(55, 13);
            this.lblAracTipi.TabIndex = 0;
            this.lblAracTipi.Text = "Araç Tipi :";
            // 
            // pnlAracBilgileri
            // 
            this.pnlAracBilgileri.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlAracBilgileri.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAracBilgileri.Controls.Add(this.txtTeslimDurumu);
            this.pnlAracBilgileri.Controls.Add(this.txtDonusKm);
            this.pnlAracBilgileri.Controls.Add(this.txtPlaka);
            this.pnlAracBilgileri.Controls.Add(this.txtAracTipi);
            this.pnlAracBilgileri.Controls.Add(this.lblTeslimDurumu);
            this.pnlAracBilgileri.Controls.Add(this.lblDonusKm);
            this.pnlAracBilgileri.Controls.Add(this.lblPlaka);
            this.pnlAracBilgileri.Controls.Add(this.lblAracTipi);
            this.pnlAracBilgileri.Location = new System.Drawing.Point(32, 218);
            this.pnlAracBilgileri.Name = "pnlAracBilgileri";
            this.pnlAracBilgileri.Size = new System.Drawing.Size(477, 100);
            this.pnlAracBilgileri.TabIndex = 10;
            // 
            // txtTeslimDurumu
            // 
            this.txtTeslimDurumu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTeslimDurumu.Location = new System.Drawing.Point(352, 55);
            this.txtTeslimDurumu.Name = "txtTeslimDurumu";
            this.txtTeslimDurumu.Size = new System.Drawing.Size(100, 20);
            this.txtTeslimDurumu.TabIndex = 7;
            // 
            // txtDonusKm
            // 
            this.txtDonusKm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDonusKm.Location = new System.Drawing.Point(352, 15);
            this.txtDonusKm.Name = "txtDonusKm";
            this.txtDonusKm.Size = new System.Drawing.Size(100, 20);
            this.txtDonusKm.TabIndex = 6;
            // 
            // txtPlaka
            // 
            this.txtPlaka.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPlaka.Location = new System.Drawing.Point(126, 56);
            this.txtPlaka.Name = "txtPlaka";
            this.txtPlaka.Size = new System.Drawing.Size(100, 20);
            this.txtPlaka.TabIndex = 5;
            // 
            // txtAracTipi
            // 
            this.txtAracTipi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAracTipi.Location = new System.Drawing.Point(126, 16);
            this.txtAracTipi.Name = "txtAracTipi";
            this.txtAracTipi.Size = new System.Drawing.Size(100, 20);
            this.txtAracTipi.TabIndex = 4;
            // 
            // lblTeslimDurumu
            // 
            this.lblTeslimDurumu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTeslimDurumu.AutoSize = true;
            this.lblTeslimDurumu.Location = new System.Drawing.Point(249, 62);
            this.lblTeslimDurumu.Name = "lblTeslimDurumu";
            this.lblTeslimDurumu.Size = new System.Drawing.Size(83, 13);
            this.lblTeslimDurumu.TabIndex = 3;
            this.lblTeslimDurumu.Text = "Teslim Durumu :";
            // 
            // lblDonusKm
            // 
            this.lblDonusKm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDonusKm.AutoSize = true;
            this.lblDonusKm.Location = new System.Drawing.Point(249, 23);
            this.lblDonusKm.Name = "lblDonusKm";
            this.lblDonusKm.Size = new System.Drawing.Size(97, 13);
            this.lblDonusKm.TabIndex = 2;
            this.lblDonusKm.Text = "Dönüş Kilometresi :";
            // 
            // lblPlaka
            // 
            this.lblPlaka.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPlaka.AutoSize = true;
            this.lblPlaka.Location = new System.Drawing.Point(24, 63);
            this.lblPlaka.Name = "lblPlaka";
            this.lblPlaka.Size = new System.Drawing.Size(40, 13);
            this.lblPlaka.TabIndex = 1;
            this.lblPlaka.Text = "Plaka :";
            // 
            // lblMüsteriBilgileri
            // 
            this.lblMüsteriBilgileri.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMüsteriBilgileri.AutoSize = true;
            this.lblMüsteriBilgileri.Location = new System.Drawing.Point(32, 22);
            this.lblMüsteriBilgileri.Name = "lblMüsteriBilgileri";
            this.lblMüsteriBilgileri.Size = new System.Drawing.Size(76, 13);
            this.lblMüsteriBilgileri.TabIndex = 9;
            this.lblMüsteriBilgileri.Text = "Müşteri Bilgileri";
            // 
            // frmDuzenle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 625);
            this.Controls.Add(this.lblKiralamaBilgileri);
            this.Controls.Add(this.lblMüsteriBilgileri);
            this.Controls.Add(this.btnİptal);
            this.Controls.Add(this.pnlKiralamaBilgileri);
            this.Controls.Add(this.btnKaydet);
            this.Controls.Add(this.lblAracBilgileri);
            this.Controls.Add(this.pnlMüsteriBilgileri);
            this.Controls.Add(this.pnlAracBilgileri);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmDuzenle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kayıt Düzenle";
            this.Load += new System.EventHandler(this.frmDuzenle_Load);
            this.pnlKiralamaBilgileri.ResumeLayout(false);
            this.pnlKiralamaBilgileri.PerformLayout();
            this.pnlMüsteriBilgileri.ResumeLayout(false);
            this.pnlMüsteriBilgileri.PerformLayout();
            this.pnlAracBilgileri.ResumeLayout(false);
            this.pnlAracBilgileri.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnİptal;
        private System.Windows.Forms.Label lblToplamUcret;
        private System.Windows.Forms.Label lblToplamParaBirimi;
        private System.Windows.Forms.RadioButton rButtonTL;
        private System.Windows.Forms.RadioButton rButtonDolar;
        private System.Windows.Forms.RadioButton rButtonEuro;
        private System.Windows.Forms.Label lblParaBirimi;
        private System.Windows.Forms.Label lblToplam;
        private System.Windows.Forms.DateTimePicker dTBitisTarihi;
        private System.Windows.Forms.Panel pnlKiralamaBilgileri;
        private System.Windows.Forms.DateTimePicker dTBaslangicTarihi;
        private System.Windows.Forms.TextBox txtKiralamaUcreti;
        private System.Windows.Forms.TextBox txtKiralamaSuresi;
        private System.Windows.Forms.TextBox txtEkstraUcretler;
        private System.Windows.Forms.Label lblEkstraUcretler;
        private System.Windows.Forms.Label lblKiralamaSuresi;
        private System.Windows.Forms.Label lblBaslangicTarihi;
        private System.Windows.Forms.Label lblBitisTarihi;
        private System.Windows.Forms.Label lblKiralamaUcreti;
        private System.Windows.Forms.Label lblKiralamaBilgileri;
        private System.Windows.Forms.Button btnKaydet;
        private System.Windows.Forms.Label lblAracBilgileri;
        private System.Windows.Forms.Panel pnlMüsteriBilgileri;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.TextBox txtMusteriSoyadi;
        private System.Windows.Forms.TextBox txtTelNo;
        private System.Windows.Forms.TextBox txtUyruk;
        private System.Windows.Forms.TextBox txtMusteriAdi;
        private System.Windows.Forms.Label lblUyruk;
        private System.Windows.Forms.Label lblMail;
        private System.Windows.Forms.Label lblTelNo;
        private System.Windows.Forms.Label lblMüsteriSoyadi;
        private System.Windows.Forms.Label lblMüsteriAdi;
        private System.Windows.Forms.Label lblAracTipi;
        private System.Windows.Forms.Panel pnlAracBilgileri;
        private System.Windows.Forms.TextBox txtTeslimDurumu;
        private System.Windows.Forms.TextBox txtDonusKm;
        private System.Windows.Forms.TextBox txtPlaka;
        private System.Windows.Forms.TextBox txtAracTipi;
        private System.Windows.Forms.Label lblTeslimDurumu;
        private System.Windows.Forms.Label lblDonusKm;
        private System.Windows.Forms.Label lblPlaka;
        private System.Windows.Forms.Label lblMüsteriBilgileri;
    }
}